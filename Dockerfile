FROM ubuntu:xenial

WORKDIR /usr/local/src/

ENV SERVER_NAME='jira-slack-dev-sprint-5and6.herokuapp.com'

ENV FASTCGI_HOST=localhost
ENV FASTCGI_PORT=9000
ENV DOCUMENT_ROOT=/usr/local/src/public
ENV UPLOAD_MAX_FILESIZE=50M
ENV FASTCGI_HOST=127.0.0.1
ENV FASTCGI_PORT=9000


RUN apt-get update -yqq \
    && apt-get install -yqq nginx gettext-base

RUN echo "#!/bin/sh" > /entrypoint.sh && \
echo "rm /etc/nginx/sites-enabled/default"  >> /entrypoint.sh  && \
echo "DOLLAR='$' envsubst < /etc/nginx/conf.d/default.conf.template > /etc/nginx/sites-enabled/default" >> /entrypoint.sh  && \
echo "exec /usr/bin/supervisord -n" >> /entrypoint.sh && \
chmod -R 777 /entrypoint.sh

ENV ITDEVGROUP_TIME_ZONE Europe/Kiev

RUN apt-get update -yqq \
    && apt-get install -yqq \
	ca-certificates \
    gcc \
    wget \
    curl \
    zip \
    supervisor \
    && rm -rf /var/lib/apt/lists/* \
	&& rm -rf /tmp/* \
	&& apt-get clean -yqq

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade \
 	&& DEBIAN_FRONTEND=noninteractive apt-get -y install python-software-properties \
 	&& DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common \
 	&& DEBIAN_FRONTEND=noninteractive LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php \
 	&& rm -rf /var/lib/apt/lists/* \
	&& rm -rf /tmp/* \
	&& apt-get clean -yqq

RUN apt-get update -yqq && apt-get install -yqq php7.2 \
	php7.2-pgsql \
    	php7.2-opcache \
    	php7.2-common \
    	php7.2-mbstring \
    	php7.2-soap \
    	php7.2-cli \
    	php7.2-intl \
    	php7.2-json \
    	php7.2-xsl \
    	php7.2-imap \
    	php7.2-ldap \
    	php7.2-curl \
    	php7.2-gd  \
		php7.2-zip  \
    	php7.2-dev \
		php7.2-fpm \
    	php-igbinary \
    	php7.2-redis \
    	php7.2-bcmath \
    	php-mongodb \
		php-zmq \
		php7.2-apcu \
        && apt-get install pkg-config \
        && apt-get install -y -q --no-install-recommends \
           ssmtp \
        && rm -rf /var/lib/apt/lists/* \
		&& rm -rf /tmp/* \
		&& apt-get clean -yqq

RUN echo $ITDEVGROUP_TIME_ZONE > /etc/timezone \
    && echo "date.timezone=$ITDEVGROUP_TIME_ZONE" > /etc/php/7.2/cli/conf.d/timezone.ini

# Download browscap.ini
RUN mkdir /var/lib/browscap \
    && wget http://browscap.org/stream?q=BrowsCapINI -O /var/lib/browscap/browscap.ini

## Install composer globally
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer -d=/var/www \
	&& mkdir -p /var/www/.composer && chown -R www-data /var/www/.composer
       USER www-data
       RUN composer global require hirak/prestissimo \
        && rm -rf /var/www/.composer/cache
       USER root
       RUN composer global require hirak/prestissimo \
        && rm -rf /root/.composer/cache

COPY ./stack/php-conf/php.ini /etc/php/7.2/cli/php.ini
COPY ./stack/php-conf/php-fpm.ini /etc/php/7.2/fpm/php.ini
COPY ./stack/php-conf/php-fpm.conf /etc/php/7.2/fpm/php-fpm.conf
COPY ./stack/php-conf/www.conf /etc/php/7.2/fpm/pool.d/www.conf



RUN usermod -u 1000 www-data
RUN usermod -aG staff,users,www-data www-data
# Reconfigure system time
RUN  dpkg-reconfigure -f noninteractive tzdata

COPY ./src ./
COPY ./stack/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY ./stack/supervisord/conf /etc/supervisor/conf.d/

RUN composer install \
	&& cp .env.example .env \
	&& php artisan key:generate \
	&& php artisan config:clear \
	&& php artisan cache:clear \
	&& php artisan view:clear

EXPOSE $PORT

CMD ["/entrypoint.sh", "sh"]
